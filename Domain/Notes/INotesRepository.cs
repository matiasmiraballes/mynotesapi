﻿using Domain.Specifications;

namespace Domain.Notes
{
    public interface INotesRepository
    {
        Task<List<Note>> GetAllUserNotesAsync(QuerySpecification queryOptions, CancellationToken cancellationToken);
        Task<Note?> GetUserNoteByIdAsync(NoteId id, CancellationToken cancellationToken);
        Task AddAsync(Note note, CancellationToken cancellationToken);
        Task DeleteUserNoteAsync(Note note, CancellationToken cancellationToken);
        Task UpdateUserNoteAsync(Note note, CancellationToken cancellationToken);
        Task<int> CountUserNotes(CancellationToken cancellationToken);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
