﻿namespace Domain.Notes
{
    public readonly record struct NoteId(Guid Value);
}
