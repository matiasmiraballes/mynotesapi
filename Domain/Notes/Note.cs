﻿using Domain.Primitives;
using Domain.Users;
using Domain.ValueObjects;
using ErrorOr;
using Domain.DomainErrors;

namespace Domain.Notes
{
    public sealed class Note : AggregateRoot
    {
        public Note(NoteId id, string title, string description, UserEmail ownerEmail, List<SharedWith> sharedWith, DateTime createdAt, DateTime updatedAt)
        {
            Id = id;
            Title = title;
            Description = description;
            OwnerEmail = ownerEmail;
            SharedWith = sharedWith;
            CreatedAt = createdAt;
            UpdatedAt = updatedAt;
        }

        private Note() { } // private empty constructor for EF

        public NoteId Id { get; private set; }
        public string Title { get; private set; } = string.Empty;
        public string Description { get; private set; }
        public UserEmail OwnerEmail { get; private set; }
        public List<SharedWith> SharedWith { get; private set; } = new List<SharedWith>();
        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }

        public static ErrorOr<Note> Create(NoteId id, string title, string description, UserEmail ownerEmail, List<SharedWith> sharedWith, DateTime createdAt, DateTime updatedAt)
        {
            if (string.IsNullOrEmpty(title.Trim()))
            {
                return Errors.Notes.EmptyNoteTitle;
            }

            return new Note(id, title.Trim(), description.Trim(), ownerEmail, sharedWith, createdAt, updatedAt);
        }
    }
}
