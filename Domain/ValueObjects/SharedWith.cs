﻿using Domain.Users;

namespace Domain.ValueObjects
{
    public partial record SharedWith
    {
        private SharedWith(UserEmail userEmail, DateTime sharedAt)
        {
            UserEmail = userEmail;
            SharedAt = sharedAt;
        }

        public UserEmail UserEmail { get; set; }
        public DateTime? SharedAt { get; set; }

        public static SharedWith? Create(UserEmail userEmail, DateTime sharedAt)
        {
            return new SharedWith(userEmail, sharedAt);
        }
    }
}
