﻿using Domain.Primitives;
using Domain.Shared;
using ErrorOr;

namespace Domain.Users
{
    public sealed class User : AggregateRoot
    {
        private const int MAX_NAME_LENGTH = 50;
        private const int MAX_EMAIL_LENGTH = 50;

        public User(UserEmail email, string name, DateTime createdAt, DateTime updatedAt)
        {
            Email = email;
            Name = name;
            CreatedAt = createdAt;
            UpdatedAt = updatedAt;
        }

        private User() { }

        public UserEmail Email { get; private set; }
        public string Name { get; private set; } = string.Empty;
        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }

        public static ErrorOr<User> Create(UserEmail email, string name, DateTime createdAt, DateTime updatedAt)
        {
            if (string.IsNullOrEmpty(email.Value))
                return Error.Validation("User.Email", "Email cannot be empty");

            if (email.Value.Length > MAX_EMAIL_LENGTH)
                return Error.Validation("User.Email", "Email length exceeded");

            if (!RegexUtilities.IsValidEmail(email.Value))
                return Error.Validation("User.Email", "Email must have valid format");


            if (string.IsNullOrEmpty(name)) 
                return Error.Validation("User.Name", "Name cannot be empty");

            if (name.Length > MAX_NAME_LENGTH)
                return Error.Validation("User.Name", "Name length exceeded");


            return new User(email, name, createdAt, updatedAt);
        }
    }
}
