﻿namespace Domain.Users
{
    public readonly record struct UserEmail(string Value);
}
