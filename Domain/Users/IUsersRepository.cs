﻿namespace Domain.Users
{
    public interface IUsersRepository
    {
        Task<User?> GetByEmailAsync(UserEmail email, CancellationToken cancellationToken);
        Task AddAsync(User user, CancellationToken cancellationToken);
        Task<bool> ExistsAsync(UserEmail email, CancellationToken cancellationToken);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
