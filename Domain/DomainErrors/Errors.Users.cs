﻿using ErrorOr;

namespace Domain.DomainErrors
{
    public static partial class Errors
    {
        public static class Users
        {
            public static Error XUserIdHeaderRequired => Error.Unauthorized("User.Email", "X-User-Id header is required.");
            public static Error NotFound(string email) => Error.NotFound("User.NotFound", $"User with email {email} was not found.");
        }
    }
}
