﻿using ErrorOr;

namespace Domain.DomainErrors
{
    public static partial class Errors
    {
        public static class General
        {
            public static Error NotFound(string entityName) => Error.NotFound("General.NotFound", $"{entityName} was not found.");
            public static Error InvalidQueryFilters => Error.Validation("General.QueryFilters", $"Invalid query filters.");
        }
    }
}
