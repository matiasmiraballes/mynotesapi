﻿using ErrorOr;

namespace Domain.DomainErrors
{
    public static partial class Errors
    {
        public static class Notes
        {
            public static Error OwnerNotFound => Error.Validation("Note.OwnerId", "Note owner not found.");
            public static Error EmptyNoteTitle => Error.Validation("Note.OwnerId", "Note title cannot be empty.");
            public static Error NoteSharedToOwner => Error.Validation("Note.SharedWith", "Note cannot be shared to itself.");
            public static Error NoteSharedToInvalidUser(string userId) => Error.Validation("Note.SharedWith", $"User {userId} not found.");
            public static Error SharedWithBadFormat => Error.Validation("Note.SharedWith", "Bad format.");
            public static Error SharedWithDuplicatedUser => Error.Validation("Note.SharedWith", "Duplicated user.");
            public static Error NotFound(string id) => Error.NotFound("Note.NotFound", $"Note with id {id} was not found.");
            public static Error Unauthorized => Error.Unauthorized("Note", $"Cannot modify non owned notes");
        }
    }
}
