﻿namespace Domain.Specifications;

public class QuerySpecification
{
    public int? limit { get; set; }
    public int? offset { get; set; }
    public string? filter { get; set; }

    public bool IsNotSet()
    {
        return limit is null && offset is null && filter is null;
    }

    public bool IsValid()
    {
        if (limit is not null && limit < 0)
        {
            return false;
        }

        if (offset is not null && offset < 0)
        {
            return false;
        }

        return true;
    }
}
