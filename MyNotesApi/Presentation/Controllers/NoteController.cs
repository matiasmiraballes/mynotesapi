﻿using Application.Notes.Count;
using Application.Notes.Create;
using Application.Notes.Delete;
using Application.Notes.GetAll;
using Application.Notes.GetById;
using Application.Notes.Update;
using Domain.Specifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MyNotesApi.Presentation.Contracts;

namespace MyNotesApi.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NoteController : ControllerBase
    {
        private readonly ISender _mediator;

        public NoteController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QuerySpecification queryOptions)
        {
            var result = await _mediator.Send(new GetAllNotesQuery(queryOptions));

            return result.Match(
                response => Ok(response),
                errors => errors.First().ToActionResult()
            );
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var result = await _mediator.Send(new GetNoteByIdQuery(id));

            return result.Match(
                response => Ok(response),
                errors => errors.First().ToActionResult()
            );
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateNoteCommand command)
        {
            var result = await _mediator.Send(
                new CreateNoteCommand(command.Title, command.Description, command.SharedWith)
            );

            return result.Match(
                response => Ok(response),
                errors => errors.First().ToActionResult()
            );
        }

        [HttpPut]
        public async Task<IActionResult> Put(UpdateNoteCommand command)
        {
            var result = await _mediator.Send(
                new UpdateNoteCommand(command.Id, command.Title, command.Description, command.OwnerEmail, command.SharedWith)
            );

            return result.Match(
                response => Ok(response),
                errors => errors.First().ToActionResult()
            );
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _mediator.Send(
                new DeleteNoteCommand(id)
            );

            return result.Match(
                response => Ok(response),
                errors => errors.First().ToActionResult()
            );
        }

        [HttpGet("count")]
        public async Task<IActionResult> GetCount()
        {
            var result = await _mediator.Send(
                new GetNotesCountQuery()
            );

            return result.Match(
                response => Ok(response),
                errors => errors.First().ToActionResult()
            );
        }
    }
}
