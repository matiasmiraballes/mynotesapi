﻿using Application.Sources.GetAll;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MyNotesApi.Presentation.Contracts;

namespace MyNotesApi.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SourceController : ControllerBase
    {
        private readonly ISender _mediator;

        public SourceController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _mediator.Send(new GetAllSourcesQuery());

            return result.Match(
                response => Ok(response),
                errors => errors.First().ToActionResult()
            );
        }
    }
}
