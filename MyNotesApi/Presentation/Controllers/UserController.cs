﻿using Application.Common.HttpHeaders;
using Application.Users.Create;
using Application.Users.Exists;
using Application.Users.GetUser;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MyNotesApi.Presentation.Contracts;
using System.Linq;

namespace MyNotesApi.Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ISender _mediator;

        public UserController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            if (Request.Headers[CustomHttpHeaders.XUserId].Count != 1)
            {
                return Unauthorized();
            }

            var result = await _mediator.Send(new GetUserQuery());

            return result.Match(
                response => Ok(response),
                errors => errors.First().ToActionResult()
            );
        }

        [HttpGet("Exists/{email}")]
        public async Task<IActionResult> UserExists(string email)
        {
            if (Request.Headers[CustomHttpHeaders.XUserId].Count != 1)
            {
                return Unauthorized();
            }

            var result = await _mediator.Send(new UserExistsQuery(email));

            return result.Match(
                response => Ok(response),
                errors => errors.First().ToActionResult()
            );
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateUserCommand command)
        {
            var result = await _mediator.Send(
                new CreateUserCommand(command.Name)
            );

            return result.Match(
                response => Ok(response),
                errors => errors.First().ToActionResult()
            );
        }
    }
}
