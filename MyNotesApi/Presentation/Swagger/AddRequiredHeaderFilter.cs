﻿using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace MyNotesApi.Presentation.Swagger
{
    public class AddRequiredHeaderFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<OpenApiParameter>();

            operation.Parameters.Add(new OpenApiParameter()
            {
                Name = "X-User-Id",
                Description = "User Identifier",
                In = ParameterLocation.Header,
                //Schema = new OpenApiSchema() { Type = "String" },
                Required = true,
                Example = new OpenApiString("user1@mail.com")
            });

            operation.Parameters.Add(new OpenApiParameter()
            {
                Name = "X-Datasource",
                Description = "Datasource id: 0: Default, 1: InMemory, 2:Supabase",
                In = ParameterLocation.Header,
                //Schema = new OpenApiSchema() { Type = "String" },
                Required = true,
                Example = new OpenApiString("0")
            });
        }
    }
}
