﻿namespace MyNotesApi.Presentation.Contracts;

public record ErrorResponse(
    string Code,
    string Description
);
