﻿using Infrastructure.Persistence.Providers.InMemory.DataClient;
using Infrastructure.Persistence.Providers.InMemory.DbModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MyNotesApi.IntegrationTests
{
    public class CustomWebApplicationFactory : WebApplicationFactory<Program>
    {
        private static object lockObj = new object();
        private static bool dataSeeded = false;

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                // We can remove dependencies and inject mocks with the following approach.
                //var descriptor = services.SingleOrDefault(
                //    d => d.ServiceType ==
                //        typeof(DbContextOptions<InMemoryDbContext>));

                //if (descriptor != null)
                //{
                //    services.Remove(descriptor);
                //}

                // Use in-memory database for testing.
                //services.AddDbContext<InMemoryDbContext>(options =>
                //{
                //    options.UseInMemoryDatabase("InMemoryDbForTesting");
                //});

                // Seed mock data into the in-memory database.
                lock (lockObj)
                {
                    if (!dataSeeded)
                    {
                        SeedMockData(services);
                        dataSeeded = true;
                    }
                }
            });
        }

        private static void SeedMockData(IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();

            using (var scope = serviceProvider.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                var context = scopedServices.GetRequiredService<InMemoryDbContext>();

                // Ensure the database is created.
                context.Database.EnsureCreated();

                // Seed mock user data.
                context.Users.AddRange(
                    new InMemoryUser
                    {
                        Email = "user1@mail.com",
                        Name = "user1",
                        CreatedAt = DateTime.UtcNow,
                        UpdatedAt = DateTime.UtcNow
                    },
                    new InMemoryUser
                    {
                        Email = "user2@mail.com",
                        Name = "user2",
                        CreatedAt = DateTime.UtcNow,
                        UpdatedAt = DateTime.UtcNow
                    });

                context.Notes.AddRange(
                    new InMemoryNote 
                    {
                        Id = Guid.NewGuid(),
                        Title = "Note 1 Title",
                        Description = "Note 1 Description",
                        OwnerEmail = "user1@mail.com"
                    }
                );

                context.SaveChanges();
            }
        }
    }
}
