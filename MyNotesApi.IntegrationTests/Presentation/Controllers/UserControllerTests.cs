﻿using Application.Users.Common;
using MyNotesApi.Presentation.Contracts;
using System.Net;
using System.Text;
using System.Text.Json;
using Xunit;

namespace MyNotesApi.IntegrationTests.Presentation.Controllers
{
    public class UserControllerTests
    {
        private CustomWebApplicationFactory _factory;
        private HttpClient _client;

        public UserControllerTests()
        {
            _factory = new CustomWebApplicationFactory();
            _client = _factory.CreateClient();
        }

        [Fact]
        public async Task T000_Get_UnauthorizedUser_ShouldGet401()
        {
            // Arrange
            var request = new HttpRequestMessage(HttpMethod.Get, "/User");

            // Act
            var response = await _client.SendAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Fact]
        public async Task T010_Get_ValidRequestForNonExistentUser_ShouldReturn404Response()
        {
            // Arrange
            var nonExistingUserEmail = "nonexistentuser@mail.com";
            var request = new HttpRequestMessage(HttpMethod.Get, "/User");
            request.Headers.Add("X-User-Id", nonExistingUserEmail);
            request.Headers.Add("X-Datasource", "0");

            // Act
            var response = await _client.SendAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            var responseBody = await response.Content.ReadAsStringAsync();
            var errorResponse = JsonSerializer.Deserialize<ErrorResponse>(responseBody, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });

            Assert.Equal("User.NotFound", errorResponse.Code);
            Assert.Equal($"User with email {nonExistingUserEmail} was not found.", errorResponse.Description);
        }

        [Fact]
        public async Task T020_Get_ValidRequest_ShouldReturnUserData()
        {
            // Arrange
            var request = new HttpRequestMessage(HttpMethod.Get, "/User");
            request.Headers.Add("X-User-Id", "user1@mail.com");
            request.Headers.Add("X-Datasource", "0");

            // Act
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            // Assert
            var responseBody = await response.Content.ReadAsStringAsync();
            var userResponse = JsonSerializer.Deserialize<UserResponse>(responseBody, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });

            Assert.Equal("user1@mail.com", userResponse.Email);
            Assert.NotEmpty(userResponse.Name);
        }

        [Fact]
        public async Task T100_Post_RequestWithoutXUserIdHeader_ShouldReturn401Response()
        {
            // Arrange
            var request = new HttpRequestMessage(HttpMethod.Post, "/User")
            {
                Content = new StringContent(JsonSerializer.Serialize(new { name = "newUser" }), Encoding.UTF8, "application/json")
            };

            // Act
            var response = await _client.SendAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);

            var responseBody = await response.Content.ReadAsStringAsync();
            var errorResponse = JsonSerializer.Deserialize<ErrorResponse>(responseBody, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });

            Assert.Equal("User.Email", errorResponse.Code);
            Assert.Equal("X-User-Id header is required.", errorResponse.Description);
        }

        [Fact]
        public async Task T110_Post_RequestWithValidXUserIdHeader_ShouldReturn200WithCreatedEmail()
        {
            // Arrange
            var request = new HttpRequestMessage(HttpMethod.Post, "/User")
            {
                Headers =
                {
                    { "X-User-Id", "newUser110@mail.com" },
                    { "X-Datasource", "0" }
                },
                Content = new StringContent(JsonSerializer.Serialize(new { name = "newUser110" }), Encoding.UTF8, "application/json")
            };

            // Act
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            // Assert
            var responseBody = await response.Content.ReadAsStringAsync();
            Assert.Equal("newUser110@mail.com", responseBody);
        }


        [Fact]
        public async Task T120_Post_AfterCreatingUser_GetUserEndpointShouldReturnUserData()
        {
            // Arrange
            // Perform the actions of the previous test to create a user
            var createRequest = new HttpRequestMessage(HttpMethod.Post, "/User")
            {
                Headers =
                {
                    { "X-User-Id", "newUser120@mail.com" },
                    { "X-Datasource", "0" }
                },
                Content = new StringContent(JsonSerializer.Serialize(new { name = "newUser120" }), Encoding.UTF8, "application/json")
            };

            var createResponse = await _client.SendAsync(createRequest);
            createResponse.EnsureSuccessStatusCode();

            // Act (perform the GET request after creating the user)
            var getRequest = new HttpRequestMessage(HttpMethod.Get, "/User")
            {
                Headers =
                {
                    { "X-User-Id", "newUser120@mail.com" },
                    { "X-Datasource", "0" }
                }
            };

            var getResponse = await _client.SendAsync(getRequest);
            getResponse.EnsureSuccessStatusCode();

            // Assert
            var responseBody = await getResponse.Content.ReadAsStringAsync();
            var userResponse = JsonSerializer.Deserialize<UserResponse>(responseBody, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });

            // Validate the returned user data
            Assert.Equal("newUser120@mail.com", userResponse.Email);
            Assert.Equal("newUser120", userResponse.Name);
        }

        [Fact]
        public async Task T200_UserExists_RequestWithoutXUserIdHeader_ShouldReturn401Response()
        {
            // Arrange
            var request = new HttpRequestMessage(HttpMethod.Get, "/User/Exists/user2@mail.com");

            // Act
            var response = await _client.SendAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }


        [Fact]
        public async Task T210_UserExists_RequestWithValidXUserIdHeaderAndExistingEmail_ShouldReturn200WithTrue()
        {
            // Arrange
            var request = new HttpRequestMessage(HttpMethod.Get, "/User/Exists/user2@mail.com")
            {
                Headers =
                {
                    { "X-User-Id", "user1@mail.com" },
                    { "X-Datasource", "0" }
                }
            };

            // Act
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            // Assert
            var responseBody = await response.Content.ReadAsStringAsync();
            Assert.Equal("true", responseBody.ToLower());
        }


        [Fact]
        public async Task RequestWithValidXUserIdHeaderAndNonExistentEmail_ShouldReturn200WithFalse()
        {
            // Arrange
            var request = new HttpRequestMessage(HttpMethod.Get, "/User/Exists/nonExistentUser@mail.com")
            {
                Headers =
                {
                    { "X-User-Id", "user1@mail.com" },
                    { "X-Datasource", "0" }
                }
            };

            // Act
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            // Assert
            var responseBody = await response.Content.ReadAsStringAsync();
            Assert.Equal("false", responseBody.ToLower());
        }
    }
}
