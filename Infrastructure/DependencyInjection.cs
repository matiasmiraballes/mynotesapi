﻿using Domain.Notes;
using Domain.Users;
using Infrastructure.Persistence.Providers;
using Infrastructure.Persistence.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration) 
        {
            services.AddPersistence(configuration);
            return services;
        }

        private static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDataProviderFactory(configuration);
            services.AddScoped<INotesRepository, NotesRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            return services;
        }
    }
}
