﻿namespace Infrastructure.Persistence.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException() { }
        public EntityNotFoundException(string message) : base(message) { }
        public EntityNotFoundException(string entityType, Guid id) : base($"{entityType} with id {id} was not found.") { }
        public EntityNotFoundException(string message, Exception inner) : base(message, inner) { }
    }
}
