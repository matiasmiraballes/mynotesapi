﻿namespace Infrastructure.Persistence.Exceptions
{
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException() { }
        public UserNotFoundException(string email) : base($"User with email {email} was not found.") { }
        public UserNotFoundException(string message, Exception inner) : base(message, inner) { }
    }
}
