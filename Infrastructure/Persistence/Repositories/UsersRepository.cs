﻿using Domain.Users;
using Infrastructure.Persistence.Providers;

namespace Infrastructure.Persistence.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly IDataProvider _provider;
        public UsersRepository(IDataProviderFactory providerFactory)
        {
            _provider = providerFactory?.GetProvider() ?? throw new ArgumentNullException(nameof(providerFactory));
        }

        public Task AddAsync(User user, CancellationToken cancellationToken)
        {
            return _provider.Users.AddAsync(user, cancellationToken);
        }

        public Task<User?> GetByEmailAsync(UserEmail email, CancellationToken cancellationToken)
        {
            return _provider.Users.GetByEmailAsync(email, cancellationToken);
        }

        public Task<bool> ExistsAsync(UserEmail email, CancellationToken cancellationToken)
        {
            return _provider.Users.ExistsAsync(email, cancellationToken);
        }


        //public Task DeleteAsync(UserId userId, CancellationToken cancellationToken)
        //{
        //    return _provider.Users.DeleteAsync(userId, cancellationToken);
        //}

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return _provider.Users.SaveChangesAsync(cancellationToken);
        }
    }
}
