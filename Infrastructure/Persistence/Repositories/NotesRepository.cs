﻿using Domain.Notes;
using Domain.Specifications;
using Infrastructure.Persistence.Providers;

namespace Infrastructure.Persistence.Repositories
{
    public class NotesRepository : INotesRepository
    {
        private readonly IDataProvider _provider;

        public NotesRepository(IDataProviderFactory providerFactory)
        {
            _provider = providerFactory?.GetProvider() ?? throw new ArgumentNullException(nameof(providerFactory));
        }

        public Task<List<Note>> GetAllUserNotesAsync(QuerySpecification queryOptions, CancellationToken cancellationToken)
        {
            return _provider.Notes.GetAllUserNotesAsync(queryOptions, cancellationToken);
        }

        public Task<Note?> GetUserNoteByIdAsync(NoteId id, CancellationToken cancellationToken)
        {
            return _provider.Notes.GetUserNoteByIdAsync(id, cancellationToken);
        }

        public Task AddAsync(Note note, CancellationToken cancellationToken)
        {
            return _provider.Notes.AddAsync(note, cancellationToken);
        }

        public Task UpdateUserNoteAsync(Note note, CancellationToken cancellationToken)
        {
            return _provider.Notes.UpdateUserNoteAsync(note, cancellationToken);
        }

        public Task DeleteUserNoteAsync(Note note, CancellationToken cancellationToken)
        {
            return _provider.Notes.DeleteUserNoteAsync(note, cancellationToken);
        }

        public Task<int> CountUserNotes(CancellationToken cancellationToken)
        {
            return _provider.Notes.CountUserNotes(cancellationToken);
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return _provider.Notes.SaveChangesAsync(cancellationToken);
        }
    }
}
