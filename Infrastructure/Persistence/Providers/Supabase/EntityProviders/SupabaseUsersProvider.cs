﻿using Application.Common.UserContext;
using Domain.Users;
using Infrastructure.Persistence.Providers.Supabase.DbModels;
using Supabase;

namespace Infrastructure.Persistence.Providers.Supabase.EntityProviders
{
    internal class SupabaseUsersProvider : IUsersRepository
    {
        private readonly Client _context;
        private readonly IUserContext _userContext;

        public SupabaseUsersProvider(Client context, IUserContext userContext)
        {
            _context = context;
            _userContext = userContext;
        }

        public async Task<User?> GetByEmailAsync(UserEmail email, CancellationToken cancellationToken)
        {
            var result = await _context.From<SupabaseUser>()
                .Where(u => u.Email == email.Value)
                .Single(cancellationToken);

            return result?.ToModel();
        }

        public async Task AddAsync(User user, CancellationToken cancellationToken)
        {
            SupabaseUser newNote = new SupabaseUser
            {
                Email = user.Email.Value,
                Name = user.Name,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
            };

            var response = await _context.From<SupabaseUser>().Insert(newNote, null, cancellationToken);

            var createdNote = response.Models.First();
        }

        public async Task<bool> ExistsAsync(UserEmail email, CancellationToken cancellationToken)
        {
            var result = await _context.From<SupabaseUser>()
                 .Where(u => u.Email == email.Value)
                 .Single(cancellationToken);

            return (result != null);
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(0);
        }
    }
}
