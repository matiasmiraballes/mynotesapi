﻿using Application.Common.UserContext;
using Domain.Notes;
using Domain.Specifications;
using Domain.Users;
using Domain.ValueObjects;
using Infrastructure.Persistence.Providers.Supabase.DbModels;
using Supabase;

namespace Infrastructure.Persistence.Providers.Supabase.EntityProviders
{
    internal class SupabaseNotesProvider : INotesRepository
    {
        private readonly Client _context;
        private readonly IUserContext _userContext;

        public SupabaseNotesProvider(Client context, IUserContext userContext)
        {
            _context = context;
            _userContext = userContext;
        }

        public async Task<List<Note>> GetAllUserNotesAsync(QuerySpecification queryOptions, CancellationToken cancellationToken)
        {
            var paginationOptions = queryOptions;

            var result = await _context.From<SupabaseNote>()
                .Where(x => x.OwnerEmail == _userContext.UserEmail.Value)
                .Get();

            // TODO: Improve this code as it is quering all the data before filtering

            var notesQuery = result.Models
                .Select(supabaseNote => supabaseNote.ToModel());

            if (paginationOptions is not null)
            {
                string? filter = paginationOptions.filter;
                if (!String.IsNullOrEmpty(filter))
                    notesQuery = notesQuery.Where(n => n.Title.ToLower().Contains(filter.ToLower())
                        || (n.Description != null && n.Description.ToLower().Contains(filter.ToLower())
                    ));

                if (paginationOptions.offset is not null)
                    notesQuery = notesQuery.Skip((int)paginationOptions.offset);

                if (paginationOptions.limit is not null)
                    notesQuery = notesQuery.Take((int)paginationOptions.limit);
            }

            var notesList = notesQuery.ToList();
            if (notesList.Count() == 0)
                return notesList;

            List<SupabaseSharedNote> sharedNotes = (await _context.From<SupabaseSharedNote>().Get()).Models;

            List<Note> notesWithSharedNotes = notesList.Select(note =>
            {
                List<SharedWith> sharedWith = sharedNotes
                    .Where(sn => sn.NoteId == note.Id.Value)
                    .Select(sn => SharedWith.Create(new UserEmail(sn.UserEmail), sn.CreatedAt)!)
                    .ToList();

                return new Note(
                    note.Id,
                    note.Title,
                    note.Description,
                    note.OwnerEmail,
                    sharedWith,
                    note.CreatedAt,
                    note.UpdatedAt
                );
            }).ToList();

            return notesWithSharedNotes;
        }

        public async Task<Note?> GetUserNoteByIdAsync(NoteId id, CancellationToken cancellationToken)
        {
            var result = await _context.From<SupabaseNote>()
                .Where(n => n.Id == id.Value)
                .Where(x => x.OwnerEmail == _userContext.UserEmail.Value)
                .Single();

            List<SupabaseSharedNote> sharedNotes = (await _context.From<SupabaseSharedNote>().Get()).Models;
            var noteSharedWith = sharedNotes.Where(sn => sn.NoteId == result.Id)
                .Select(sn => SharedWith.Create(new UserEmail(sn.UserEmail), sn.CreatedAt)!)
                .ToList();

            var note = result?.ToModel(noteSharedWith);

            return note;
        }

        public async Task AddAsync(Note note, CancellationToken cancellationToken)
        {
            SupabaseNote newNote = new SupabaseNote
            {
                Title = note.Title,
                Description = note.Description,
                OwnerEmail = note.OwnerEmail.Value,
                CreatedAt = note.CreatedAt,
                UpdatedAt = DateTime.UtcNow,
            };

            var response = await _context.From<SupabaseNote>().Insert(newNote);
            //var createdNote = response.Models.First();
        }
        public async Task UpdateUserNoteAsync(Note note, CancellationToken cancellationToken)
        {
            var response = await _context.From<SupabaseNote>()
                .Where(n => n.Id == note.Id.Value)
                .Where(n => n.OwnerEmail == _userContext.UserEmail.Value)
                .Set(n => n.Title, note.Title)
                .Set(n => n.Description, note.Description)
                .Set(n => n.UpdatedAt, DateTime.UtcNow)
                .Update();

            //var updated = response.Models.First();
        }

        public async Task DeleteUserNoteAsync(Note note, CancellationToken cancellationToken)
        {
            var noteToDeleteQuery = await _context.From<SupabaseNote>()
                .Where(n => n.Id == note.Id.Value)
                .Where(n => n.OwnerEmail == _userContext.UserEmail.Value)
                .Get();

            // TODO: Re add exceptions
            //if (noteToDeleteQuery.Models.Count == 0)
            //    throw new EntityNotFoundException("Note", id);

            await _context.From<SupabaseSharedNote>()
                .Where(n => n.NoteId == note.Id.Value)
                .Delete();

            await _context.From<SupabaseNote>()
                .Where(n => n.Id == note.Id.Value)
                .Delete();
        }

        public async Task<int> CountUserNotes(CancellationToken cancellationToken)
        {
            return await _context.From<SupabaseNote>()
                .Where(x => x.OwnerEmail == _userContext.UserEmail.Value)
                .Count(Postgrest.Constants.CountType.Exact);
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(0);
        }
    }
}
