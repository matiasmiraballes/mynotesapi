﻿using Application.Common.UserContext;
using Domain.Notes;
using Domain.Users;
using Infrastructure.Persistence.Providers.Supabase.EntityProviders;
using Supabase;

namespace Infrastructure.Persistence.Providers.Supabase
{
    public class SupabaseProvider : IDataProvider
    {
        private readonly Client _dataClient;
        private readonly IUserContext _userContext;

        public SupabaseProvider(Client dataClient, IUserContext userContext)
        {
            _dataClient = dataClient ?? throw new ArgumentNullException(nameof(dataClient));
            _userContext = userContext ?? throw new ArgumentNullException(nameof(userContext));
        }

        public INotesRepository Notes => new SupabaseNotesProvider(_dataClient, _userContext);
        public IUsersRepository Users => new SupabaseUsersProvider(_dataClient, _userContext);
        public Task SaveChangesAsync() => Task.CompletedTask;
    }
}
