﻿using Postgrest.Attributes;
using Postgrest.Models;

namespace Infrastructure.Persistence.Providers.Supabase.DbModels
{
    [Table("shared_note")]
    public class SupabaseSharedNote : BaseModel
    {
        [PrimaryKey("note_id", false)]
        //[Reference(typeof(SupabaseNote))]
        public Guid NoteId { get; set; }

        [Column("user_email")]
        //[Reference(typeof(SupabaseUser))]
        public string UserEmail { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; }
    }
}
