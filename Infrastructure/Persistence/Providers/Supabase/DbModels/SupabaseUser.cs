﻿using Domain.Users;
using Postgrest.Attributes;
using Postgrest.Models;

namespace Infrastructure.Persistence.Providers.Supabase.DbModels
{
    [Table("user")]
    public class SupabaseUser : BaseModel
    {
        [PrimaryKey("email", false)]
        public string Email { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; }

        public User ToModel()
        {
            return new User
            (
                new UserEmail(Email),
                Name,
                CreatedAt,
                UpdatedAt
            );
        }
    }
}
