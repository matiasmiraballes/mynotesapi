﻿using Domain.Notes;
using Postgrest.Models;
using Postgrest.Attributes;
using Domain.Users;
using Domain.ValueObjects;

namespace Infrastructure.Persistence.Providers.Supabase.DbModels
{
    [Table("note")]
    public class SupabaseNote : BaseModel
    {
        [PrimaryKey("id", false)]
        public Guid Id { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("description")]
        public string? Description { get; set; }

        [Column("owner_email")]
        //[Reference(typeof(SupabaseSharedNote))]
        public string OwnerEmail { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; }

        public Note ToModel(List<SharedWith>? sharedWith = null)
        {
            sharedWith ??= new List<SharedWith>();

            return new Note(
                new NoteId(Id),
                Title,
                Description,
                new UserEmail(OwnerEmail),
                sharedWith,
                CreatedAt,
                UpdatedAt
            );
        }
    }

    public static class NoteExtension
    {
        public static SupabaseNote FromModel(this Note note)
        {
            return new SupabaseNote()
            {
                Id = note.Id.Value,
                Title = note.Title,
                Description = note.Description,
                OwnerEmail = note.OwnerEmail.Value,
                CreatedAt = note.CreatedAt,
                UpdatedAt = note.UpdatedAt,
            };
        }
    }
}
