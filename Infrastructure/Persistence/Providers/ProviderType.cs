﻿namespace Infrastructure.Persistence.Providers;

public enum ProviderType
{
    Default = 0,
    InMemory = 1,
    Supabase = 2,
}
