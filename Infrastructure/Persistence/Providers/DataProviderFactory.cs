﻿using Infrastructure.Persistence.Providers.InMemory.DataClient;
using Infrastructure.Persistence.Providers.InMemory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Application.Common.UserContext;
using Supabase;
using Infrastructure.Persistence.Providers.Supabase;
using Microsoft.AspNetCore.Http;
using Application.Common.HttpHeaders;

namespace Infrastructure.Persistence.Providers
{
    public static class IServiceCollectionExtension
    {
        public static void AddDataProviderFactory(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<Client>(_ => new Client(
                configuration["Supabase:Url"],
                configuration["Supabase:Key"],
                new SupabaseOptions()
                {
                    AutoRefreshToken = true,
                }));

            services.AddDbContext<InMemoryDbContext>();
            //services.AddDbContext<InMemoryDbContext>(optionsBuilder => optionsBuilder.UseInMemoryDatabase(databaseName: "InMemoryDb"));
            //services.AddDbContext<SqlDbContext>(optionsBuilder => options.UseSqlServer(Configuration.GetConnectionString("SqlConnectionString")));

            services.AddScoped<IDataProvider, SupabaseProvider>(serviceProvider =>
            {
                Client supabaseClient = serviceProvider.GetRequiredService<Client>();
                IUserContext userContextService = serviceProvider.GetRequiredService<IUserContext>();
                return new SupabaseProvider(supabaseClient, userContextService);
            });

            services.AddScoped<IDataProvider, InMemoryProvider>(serviceProvider =>
            {
                InMemoryDbContext inMemoryClient = serviceProvider.GetRequiredService<InMemoryDbContext>();
                IUserContext userContextService = serviceProvider.GetRequiredService<IUserContext>();
                return new InMemoryProvider(inMemoryClient, userContextService);
            });

            services.AddScoped<Func<IEnumerable<IDataProvider>>>(serviceProvider => () =>
            {
                return serviceProvider.GetServices<IDataProvider>();
            });
            services.AddScoped<IDataProviderFactory, DataProviderFactory>();
        }
    }

    public interface IDataProviderFactory
    {
        IDataProvider GetProvider();
        IDataProvider GetProvider(string? providerType);
        IDataProvider GetProvider(ProviderType providerType);
    }

    public class DataProviderFactory : IDataProviderFactory
    {
        private readonly Func<IEnumerable<IDataProvider>> _factory;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly Dictionary<ProviderType, Type> _dataProviderTypesMap = new Dictionary<ProviderType, Type>()
        {
            { ProviderType.Default, typeof(InMemoryProvider) },
            { ProviderType.InMemory, typeof(InMemoryProvider) },
            { ProviderType.Supabase, typeof(SupabaseProvider) },
        };

        public DataProviderFactory(Func<IEnumerable<IDataProvider>> factory, IHttpContextAccessor httpContextAccessor)
        {
            _factory = factory;
            this._httpContextAccessor = httpContextAccessor;
        }

        public IDataProvider GetProvider()
        {
            var httpRequestHeaders = _httpContextAccessor.HttpContext!.Request.Headers;
            string? dataSource = httpRequestHeaders[CustomHttpHeaders.XDatasource];

            return GetProvider(dataSource);
        }

        public IDataProvider GetProvider(string? providerType)
        {
            if (string.IsNullOrWhiteSpace(providerType))
            {
                return GetProvider(ProviderType.Default);
            }

            int providerId;
            if (!Int32.TryParse(providerType, out providerId))
            {
                return GetProvider(ProviderType.Default);
            }

            providerId = int.Parse(providerType);
            if (!Enum.IsDefined((ProviderType)providerId))
            {
                return GetProvider(ProviderType.Default);
            }

            return GetProvider((ProviderType)providerId);
        }

        public IDataProvider GetProvider(ProviderType providerType)
        {
            var dataProviders = _factory();
            IDataProvider dataProvider = dataProviders.Where(dp => dp.GetType() == _dataProviderTypesMap[providerType]).First();
            return dataProvider;
        }
    }
}
