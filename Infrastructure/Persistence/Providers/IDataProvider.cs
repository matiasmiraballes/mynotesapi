﻿using Domain.Notes;
using Domain.Users;

namespace Infrastructure.Persistence.Providers
{
    public interface IDataProvider
    {
        INotesRepository Notes { get; }
        IUsersRepository Users { get; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
