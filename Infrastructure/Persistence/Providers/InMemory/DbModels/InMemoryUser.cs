﻿using Domain.Users;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.Persistence.Providers.InMemory.DbModels
{
    public class InMemoryUser
    {
        public InMemoryUser()
        {
            OwnedNotes = new HashSet<InMemoryNote>();
            NotesSharedToUser = new HashSet<InMemoryNote>();
        }

        [Key]
        public string Email { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        #region Navigation Properties

        public virtual ICollection<InMemoryNote> OwnedNotes { get; set; }
        public virtual ICollection<InMemoryNote> NotesSharedToUser { get; set; }

        #endregion

        public User ToModel()
        {
            return new User(new UserEmail(Email), Name, CreatedAt, UpdatedAt);
        }
    }

    public static class UserExtension
    {
        public static InMemoryUser FromModel(this User user)
        {
            return new InMemoryUser()
            {
                Email = user.Email.Value,
                Name = user.Name,
                CreatedAt = user.CreatedAt,
                UpdatedAt = user.UpdatedAt,
            };
        }
    }
}
