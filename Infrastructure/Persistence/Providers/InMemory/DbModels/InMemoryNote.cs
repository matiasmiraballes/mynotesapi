﻿using Domain.Notes;
using Domain.Users;
using Domain.ValueObjects;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure.Persistence.Providers.InMemory.DbModels
{
    public class InMemoryNote
    {
        public InMemoryNote()
        {
            SharedWith = new HashSet<InMemoryUser>();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        #region Navigation Properties
        public string OwnerEmail { get; set; }
        [ForeignKey("OwnerEmail")]
        public virtual InMemoryUser Owner { get; set; }

        public virtual ICollection<InMemoryUser> SharedWith { get; set; }
        
        #endregion

        public Note ToModel()
        {
            return new Note(
                new NoteId(Id),
                Title,
                Description,
                new UserEmail(OwnerEmail),
                new List<SharedWith>(),
                CreatedAt,
                UpdatedAt
            );
        }
    }

    public static class NoteExtension
    {
        public static InMemoryNote FromModel(this Note note)
        {
            return new InMemoryNote()
            {
                Id = note.Id.Value,
                Title = note.Title,
                Description = note.Description,
                OwnerEmail = note.OwnerEmail.Value,
                CreatedAt = note.CreatedAt,
                UpdatedAt = note.UpdatedAt,
            };
        }
    }
}
