﻿using Infrastructure.Persistence.Providers.InMemory.DbModels;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Providers.InMemory.DataClient
{
    public class InMemoryDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "InMemoryDb");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InMemoryUser>()
                .HasMany<InMemoryNote>(x => x.OwnedNotes);

            modelBuilder.Entity<InMemoryUser>()
                .HasMany<InMemoryNote>(x => x.NotesSharedToUser);

            modelBuilder.Entity<InMemoryNote>()
                .HasOne<InMemoryUser>(x => x.Owner);

            modelBuilder.Entity<InMemoryNote>()
                .HasMany<InMemoryUser>(x => x.SharedWith);
        }

        public DbSet<InMemoryNote> Notes { get; set; }
        public DbSet<InMemoryUser> Users { get; set; }
    }
}
