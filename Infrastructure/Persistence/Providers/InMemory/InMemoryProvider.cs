﻿using Application.Common.UserContext;
using Domain.Notes;
using Domain.Users;
using Infrastructure.Persistence.Providers.InMemory.DataClient;
using Infrastructure.Persistence.Providers.InMemory.EntityProviders;

namespace Infrastructure.Persistence.Providers.InMemory
{
    public class InMemoryProvider : IDataProvider
    {
        private readonly InMemoryDbContext _dataClient;
        private readonly IUserContext _userContext;
        public InMemoryProvider(InMemoryDbContext dataClient, IUserContext userContext)
        {
            _dataClient = dataClient ?? throw new ArgumentNullException(nameof(dataClient));
            _userContext = userContext ?? throw new ArgumentNullException(nameof(userContext));
        }

        public INotesRepository Notes => new InMemoryNotesProvider(_dataClient, _userContext);
        public IUsersRepository Users => new InMemoryUsersProvider(_dataClient, _userContext);
        public Task<int> SaveChangesAsync(CancellationToken cancellationToken) => _dataClient.SaveChangesAsync();
    }
}
