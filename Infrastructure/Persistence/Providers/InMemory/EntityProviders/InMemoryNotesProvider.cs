﻿using Application.Common.UserContext;
using Domain.Notes;
using Domain.Specifications;
using Infrastructure.Persistence.Exceptions;
using Infrastructure.Persistence.Providers.InMemory.DataClient;
using Infrastructure.Persistence.Providers.InMemory.DbModels;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Providers.InMemory.EntityProviders
{
    internal class InMemoryNotesProvider : INotesRepository
    {
        private readonly InMemoryDbContext _context;
        private readonly IUserContext _userContext;

        public InMemoryNotesProvider(InMemoryDbContext context, IUserContext userContext)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userContext = userContext ?? throw new ArgumentNullException(nameof(userContext));
        }

        public async Task<List<Note>> GetAllUserNotesAsync(QuerySpecification queryOptions, CancellationToken cancellationToken)
        {
            var paginationOptions = queryOptions;

            if (paginationOptions is null || paginationOptions.IsNotSet())
                return await _context.Notes
                    .AsNoTracking()
                    .Where(n => n.OwnerEmail == _userContext.UserEmail.Value)
                    .Include(n => n.SharedWith)
                    .Select(n => n.ToModel())
                    .ToListAsync(cancellationToken);

            IQueryable<InMemoryNote> query = _context.Notes
                .AsNoTracking()
                .Where(n => n.OwnerEmail == _userContext.UserEmail.Value);

            string? filter = paginationOptions.filter;
            if (!String.IsNullOrEmpty(filter))
                query = query.Where(n => n.Title.ToLower().Contains(filter.ToLower())
                    || (n.Description != null && n.Description.ToLower().Contains(filter.ToLower())
                ));

            if (paginationOptions.offset is not null)
                query = query.Skip((int)paginationOptions.offset);

            if (paginationOptions.limit is not null)
                query = query.Take((int)paginationOptions.limit);

            return await query.Include(n => n.SharedWith)
                .Select(n => n.ToModel())
                .ToListAsync(cancellationToken);
        }

        public async Task<Note?> GetUserNoteByIdAsync(NoteId id, CancellationToken cancellationToken)
        {
            var inMemoryNote = await _context.Notes
                .AsNoTracking()
                .FirstOrDefaultAsync(n => n.Id == id.Value && n.OwnerEmail == _userContext.UserEmail.Value, cancellationToken);

            return inMemoryNote?.ToModel();
        }

        /// <exception cref="UserNotFoundException">When trying to share note with invalid user.</exception>
        public async Task AddAsync(Note note, CancellationToken cancellationToken)
        {
            var inMemoryNote = note.FromModel();
            inMemoryNote.OwnerEmail = _userContext.UserEmail.Value;
            inMemoryNote.CreatedAt = DateTime.UtcNow;
            inMemoryNote.UpdatedAt = DateTime.UtcNow;

            List<InMemoryUser> sharedWith = new();
            foreach (var sharedWithUser in note.SharedWith)
            {
                sharedWith.Add(_context.Users.Find(sharedWithUser) ?? throw new UserNotFoundException(sharedWithUser.UserEmail.Value));
            }
            inMemoryNote.SharedWith = sharedWith;

            await _context.Notes.AddAsync(inMemoryNote);
        }

        /// <exception cref="EntityNotFoundException">When the entity to update is not found.</exception>
        public async Task UpdateUserNoteAsync(Note entity, CancellationToken cancellationToken)
        {
            var noteToUpdate = await _context.Notes.FindAsync(entity.Id.Value);

            if (noteToUpdate is null || noteToUpdate.OwnerEmail != _userContext.UserEmail.Value)
                throw new EntityNotFoundException("Note", entity.Id.Value);

            noteToUpdate.Title = entity.Title;
            noteToUpdate.Description = entity.Description;
            noteToUpdate.UpdatedAt = DateTime.UtcNow;

            List<InMemoryUser> sharedWith = entity.SharedWith.Count == 0 ? new List<InMemoryUser>()
                : entity.SharedWith.Select(sw => _context.Users.Find(sw)!).ToList();

            noteToUpdate.SharedWith = sharedWith;
        }

        /// <exception cref="EntityNotFoundException">When the entity to delete is not found.</exception>
        public async Task DeleteUserNoteAsync(Note note, CancellationToken cancellationToken)
        {
            InMemoryNote? entityToDelete = await _context.Notes.FindAsync(note.Id.Value);

            if (entityToDelete is null || entityToDelete.OwnerEmail != _userContext.UserEmail.Value)
                throw new EntityNotFoundException("Note not found");

            _context.Notes.Remove(entityToDelete);
        }

        public async Task<int> CountUserNotes(CancellationToken cancellationToken)
        {
            return await _context.Notes
                .Where(n => n.OwnerEmail == _userContext.UserEmail.Value)
                .CountAsync(cancellationToken);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
