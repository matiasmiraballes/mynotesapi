﻿using Application.Common.UserContext;
using Domain.Users;
using Infrastructure.Persistence.Providers.InMemory.DataClient;
using Infrastructure.Persistence.Providers.InMemory.DbModels;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Providers.InMemory.EntityProviders
{
    public class InMemoryUsersProvider : IUsersRepository
    {
        private readonly InMemoryDbContext _context;
        private readonly IUserContext _userContext;

        public InMemoryUsersProvider(InMemoryDbContext context, IUserContext userContext)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userContext = userContext ?? throw new ArgumentNullException(nameof(userContext));
        }

        public async Task<User?> GetByEmailAsync(UserEmail email, CancellationToken cancellationToken)
        {
            var inMemoryUser = await _context.Users.FirstOrDefaultAsync(u => u.Email == email.Value, cancellationToken);
            return inMemoryUser?.ToModel();
        }

        public async Task AddAsync(User user, CancellationToken cancellationToken)
        {

            var inMemoryUser = user.FromModel();
            inMemoryUser.CreatedAt = DateTime.UtcNow;
            inMemoryUser.UpdatedAt = DateTime.UtcNow;

            await _context.Users.AddAsync(inMemoryUser);

            // EF automatically updates the entity after adding it
            return;
        }

        public async Task<bool> ExistsAsync(UserEmail email, CancellationToken cancellationToken)
        {
            return await _context.Users.AnyAsync(u => u.Email == email.Value, cancellationToken);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteAsync(UserId userId, CancellationToken cancellationToken)
        {
            if(userId != _userContext.UserId)
            {
                return;
            }

            var userToDelete = await _context.Users.FindAsync(userId.Value, cancellationToken);
            if(userToDelete != null)
            {
                _context.Users.Remove(userToDelete);
            }
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
