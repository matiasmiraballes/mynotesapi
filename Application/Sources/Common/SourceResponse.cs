﻿namespace Application.Sources.Common;

public record SourceResponse(int Id, string Description);
