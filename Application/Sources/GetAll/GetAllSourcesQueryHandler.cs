﻿using Application.Sources.Common;
using ErrorOr;
using MediatR;

namespace Application.Sources.GetAll
{
    public record GetAllSourcesQuery() : IRequest<ErrorOr<List<SourceResponse>>>;

    internal sealed class GetAllSourcesQueryHandler : IRequestHandler<GetAllSourcesQuery, ErrorOr<List<SourceResponse>>>
    {
        public async Task<ErrorOr<List<SourceResponse>>> Handle(GetAllSourcesQuery request, CancellationToken cancellationToken)
        {
            List<SourceResponse> _sources = new List<SourceResponse>()
            {
                new SourceResponse(Id : 1, Description: "In-Memory Backend"),
            };

            return _sources;
        }
    }
}
