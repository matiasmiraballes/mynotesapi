﻿namespace Application.Users.Common;

public record UserResponse(
    string Email,
    string Name,
    DateTime CreatedAt,
    DateTime UpdatedAt
);
