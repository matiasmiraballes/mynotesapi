﻿using ErrorOr;
using MediatR;

namespace Application.Users.Create;

public record CreateUserCommand(string Name) : IRequest<ErrorOr<string>>;
