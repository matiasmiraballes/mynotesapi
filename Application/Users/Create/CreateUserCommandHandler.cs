﻿using Application.Common.UserContext;
using Domain.DomainErrors;
using Domain.Notes;
using Domain.Users;
using ErrorOr;
using MediatR;

namespace Application.Users.Create
{
    internal sealed class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, ErrorOr<string>>
    {
        private readonly IUsersRepository _usersRepository;
        private readonly IUserContext _userContext;

        public CreateUserCommandHandler(IUsersRepository usersRepository, IUserContext userContext)
        {
            _usersRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
            _userContext = userContext ?? throw new ArgumentNullException(nameof(userContext));
        }

        public async Task<ErrorOr<string>> Handle(CreateUserCommand command, CancellationToken cancellationToken)
        {
            var userEmail = _userContext.UserEmail;

            if (userEmail.Value == "")
                return Errors.Users.XUserIdHeaderRequired;

            var newUser = User.Create(
                userEmail,
                command.Name,
                DateTime.UtcNow,
                DateTime.UtcNow
            );

            if (newUser.IsError)
                return newUser.FirstError;

            await _usersRepository.AddAsync(newUser.Value, cancellationToken);
            await _usersRepository.SaveChangesAsync(cancellationToken);

            return newUser.Value.Email.Value;
        }
    }
}
