﻿using Application.Common.UserContext;
using Application.Users.Common;
using Domain.DomainErrors;
using Domain.Users;
using ErrorOr;
using MediatR;

namespace Application.Users.GetUser
{
    internal sealed class GetUserQueryHandler : IRequestHandler<GetUserQuery, ErrorOr<UserResponse>>
    {
        private readonly IUsersRepository _usersRepository;
        private readonly IUserContext _userContext;

        public GetUserQueryHandler(IUsersRepository usersRepository, IUserContext userContext)
        {
            _usersRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
            _userContext = userContext ?? throw new ArgumentNullException(nameof(userContext));
        }

        public async Task<ErrorOr<UserResponse>> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            var userEmail = _userContext.UserEmail;
            var userInfo = await _usersRepository.GetByEmailAsync(userEmail, cancellationToken);

            if (userInfo == null)
            {
                return Errors.Users.NotFound(userEmail.Value);
            }

            return new UserResponse(
                userInfo.Email.Value, userInfo.Name, userInfo.CreatedAt, userInfo.UpdatedAt
            );
        }
    }
}
