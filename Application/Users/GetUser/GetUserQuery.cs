﻿using Application.Users.Common;
using ErrorOr;
using MediatR;

namespace Application.Users.GetUser;

public record GetUserQuery() : IRequest<ErrorOr<UserResponse>>;
