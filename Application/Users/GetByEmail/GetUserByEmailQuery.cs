﻿using Application.Users.Common;
using ErrorOr;
using MediatR;

namespace Application.Users.GetById;

public record GetUserByEmailQuery(string Email) : IRequest<ErrorOr<UserResponse>>;
