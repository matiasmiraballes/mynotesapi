﻿using Application.Users.Common;
using Domain.Users;
using ErrorOr;
using MediatR;

namespace Application.Users.GetById;

internal sealed class GetUserByEmailQueryHandler : IRequestHandler<GetUserByEmailQuery, ErrorOr<UserResponse>>
{
    private readonly IUsersRepository _usersRepository;

    public GetUserByEmailQueryHandler(IUsersRepository usersRepository)
    {
        _usersRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
    }

    public async Task<ErrorOr<UserResponse>> Handle(GetUserByEmailQuery request, CancellationToken cancellationToken)
    {
        var user = await _usersRepository.GetByEmailAsync(new UserEmail(request.Email), cancellationToken);

        if (user == null)
        {
            return Error.NotFound("User", $"User with email {request.Email} was not found");
        }

        return new UserResponse(
            user.Email.Value, 
            user.Name,
            user.CreatedAt, 
            user.UpdatedAt
        );
    }
}
