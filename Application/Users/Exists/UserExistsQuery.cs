﻿using ErrorOr;
using MediatR;

namespace Application.Users.Exists;

public record UserExistsQuery(string email) : IRequest<ErrorOr<bool>>;
