﻿using Domain.Users;
using ErrorOr;
using MediatR;

namespace Application.Users.Exists
{
    internal sealed class UserExistsQueryHandler : IRequestHandler<UserExistsQuery, ErrorOr<bool>>
    {
        private readonly IUsersRepository _usersRepository;

        public UserExistsQueryHandler(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
        }

        public async Task<ErrorOr<bool>> Handle(UserExistsQuery request, CancellationToken cancellationToken)
        {
            return await _usersRepository.ExistsAsync(new UserEmail(request.email), cancellationToken);
        }
    }
}
