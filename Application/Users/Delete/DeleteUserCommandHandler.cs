﻿//using Domain.DomainErrors;
//using Domain.Users;
//using ErrorOr;
//using MediatR;

//namespace Application.Users.Delete
//{
//    internal sealed class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, ErrorOr<Unit>>
//    {
//        private readonly IUsersRepository _usersRepository;

//        public DeleteUserCommandHandler(IUsersRepository usersRepository)
//        {
//            _usersRepository = usersRepository;
//        }

//        public async Task<ErrorOr<Unit>> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
//        {
//            var userToDelete = await _usersRepository.GetByIdAsync(new UserId(request.id), cancellationToken);
//            if (userToDelete is null)
//            {
//                return Errors.General.NotFound("User");
//            }

//            return Unit.Value;
//        }
//    }
//}
