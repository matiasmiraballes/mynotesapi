﻿namespace Application.Common.HttpHeaders
{
    public static class CustomHttpHeaders
    {
        public static string XDatasource = "X-Datasource";
        public static string XUserId = "X-User-Id";
    }
}
