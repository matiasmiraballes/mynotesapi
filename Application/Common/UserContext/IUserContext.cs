﻿using Domain.Users;

namespace Application.Common.UserContext;

public interface IUserContext
{
    UserEmail UserEmail { get; }
}
