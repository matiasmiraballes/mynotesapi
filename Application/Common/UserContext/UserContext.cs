﻿using Application.Common.HttpHeaders;
using Domain.Users;
using Microsoft.AspNetCore.Http;

namespace Application.Common.UserContext
{
    public class UserContext : IUserContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserContext(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
        }

        // TODO: Once authentication is in place, we can get user-id from ClaimsPrincipal
        // public ClaimsPrincipal User => _httpContextAccessor.HttpContext.User;
        public UserEmail UserEmail
        {
            get {
                var httpRequestHeaders = _httpContextAccessor.HttpContext!.Request.Headers;
                string? email = httpRequestHeaders[CustomHttpHeaders.XUserId];

                if (string.IsNullOrEmpty(email))
                    return new UserEmail("");
                else
                    return new UserEmail(email);
            }
        }
    }
}
