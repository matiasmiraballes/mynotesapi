﻿using Application.Notes.Common;
using Domain.DomainErrors;
using Domain.Notes;
using ErrorOr;
using MediatR;

namespace Application.Notes.GetAll
{
    internal sealed class GetAllNotesQueryHandler : IRequestHandler<GetAllNotesQuery, ErrorOr<List<NoteResponse>>>
    {
        private readonly INotesRepository _notesRepository;

        public GetAllNotesQueryHandler(INotesRepository notesRepository)
        {
            _notesRepository = notesRepository ?? throw new ArgumentNullException(nameof(notesRepository));
        }

        public async Task<ErrorOr<List<NoteResponse>>> Handle(GetAllNotesQuery request, CancellationToken cancellationToken)
        {
            if (!request.queryOptions.IsValid())
            {
                return Errors.General.InvalidQueryFilters;
            }

            List<Note> notes = await _notesRepository.GetAllUserNotesAsync(request.queryOptions, cancellationToken);

            List<NoteResponse> notesResponse = notes.Select(n =>            
                new NoteResponse(
                    n.Id.Value,
                    n.Title,
                    n.Description,
                    n.OwnerEmail.Value,
                    n.SharedWith.Select(sw => sw.UserEmail.Value).ToList()
                )
            ).ToList();

            return notesResponse;
        }
    }
}
