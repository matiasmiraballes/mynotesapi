﻿using Application.Notes.Common;
using Domain.Specifications;
using ErrorOr;
using MediatR;

namespace Application.Notes.GetAll;

public record GetAllNotesQuery(QuerySpecification queryOptions) : IRequest<ErrorOr<List<NoteResponse>>>;
