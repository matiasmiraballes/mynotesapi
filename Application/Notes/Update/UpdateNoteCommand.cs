﻿using ErrorOr;
using MediatR;

namespace Application.Notes.Update;

public record UpdateNoteCommand(
    Guid Id,
    string Title,
    string Description,
    string OwnerEmail,
    List<string> SharedWith
) : IRequest<ErrorOr<Unit>>;
