﻿using Application.Common.UserContext;
using Domain.DomainErrors;
using Domain.Notes;
using Domain.Users;
using Domain.ValueObjects;
using ErrorOr;
using MediatR;

namespace Application.Notes.Update
{
    internal sealed class UpdateNoteCommandHandler : IRequestHandler<UpdateNoteCommand, ErrorOr<Unit>>
    {
        private readonly INotesRepository _notesRepository;
        private readonly IUsersRepository _userRepository;
        private readonly IUserContext _userContext;

        public UpdateNoteCommandHandler(INotesRepository notesRepository, IUsersRepository userRepository, IUserContext userContext)
        {
            _notesRepository = notesRepository ?? throw new ArgumentNullException(nameof(notesRepository));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userContext = userContext ?? throw new ArgumentNullException(nameof(userContext));
        }

        public async Task<ErrorOr<Unit>> Handle(UpdateNoteCommand command, CancellationToken cancellationToken)
        {
            var activeOwnerEmail = _userContext.UserEmail;

            var noteToUpdate = await _notesRepository.GetUserNoteByIdAsync(new NoteId(command.Id), cancellationToken);
            
            if (noteToUpdate == null)
            {
                return Errors.Notes.NotFound(command.Id.ToString());
            }

            if(activeOwnerEmail != noteToUpdate.OwnerEmail)
            {
                return Errors.Notes.Unauthorized;
            }

            User? noteOwner = await _userRepository.GetByEmailAsync(activeOwnerEmail, cancellationToken);
            if (noteOwner is null)
            {
                return Errors.Notes.OwnerNotFound;
            }

            List<SharedWith> sharedToUsers = new List<SharedWith>();
            foreach (var userEmail in command.SharedWith)
            {
                if (activeOwnerEmail == new UserEmail(userEmail))
                {
                    return Errors.Notes.NoteSharedToOwner;
                }

                var dbUser = await _userRepository.GetByEmailAsync(new UserEmail(userEmail), cancellationToken);
                if (dbUser is null)
                {
                    return Errors.Notes.NoteSharedToInvalidUser(userEmail.ToString());
                }

                if (SharedWith.Create(new UserEmail(userEmail), DateTime.UtcNow) is not SharedWith sharedWith)
                {
                    return Errors.Notes.SharedWithBadFormat;
                }

                if (sharedToUsers.Contains(sharedWith))
                {
                    return Errors.Notes.SharedWithDuplicatedUser;
                }

                sharedToUsers.Add(sharedWith);
            }

            var note = Note.Create(
                noteToUpdate.Id,
                command.Title.Trim(),
                command.Description.Trim(),
                noteToUpdate.OwnerEmail,
                sharedToUsers,
                DateTime.UtcNow,
                DateTime.UtcNow
            );

            await _notesRepository.UpdateUserNoteAsync(note.Value, cancellationToken);
            await _notesRepository.SaveChangesAsync(cancellationToken);
            
            return Unit.Value;
        }
    }
}
