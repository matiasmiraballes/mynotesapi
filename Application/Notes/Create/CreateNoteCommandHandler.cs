﻿using Application.Common.UserContext;
using Domain.DomainErrors;
using Domain.Notes;
using Domain.Users;
using Domain.ValueObjects;
using ErrorOr;
using MediatR;

namespace Application.Notes.Create
{
    internal sealed class CreateNoteCommandHandler : IRequestHandler<CreateNoteCommand, ErrorOr<Guid>>
    {
        private readonly INotesRepository _notesRepository;
        private readonly IUsersRepository _userRepository;
        private readonly IUserContext _userContext;

        public CreateNoteCommandHandler(INotesRepository notesRepository, IUsersRepository userRepository, IUserContext userContext)
        {
            _notesRepository = notesRepository ?? throw new ArgumentNullException(nameof(notesRepository));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userContext = userContext ?? throw new ArgumentNullException(nameof(userContext));
        }

        public async Task<ErrorOr<Guid>> Handle(CreateNoteCommand command, CancellationToken cancellationToken)
        {
            var ownerEmail = _userContext.UserEmail;

            User? noteOwner = await _userRepository.GetByEmailAsync(ownerEmail, cancellationToken);
            if(noteOwner is null)
            {
                return Errors.Notes.OwnerNotFound;
            }

            List<SharedWith> sharedToUsers = new List<SharedWith>();
            foreach (var userEmail in command.SharedWith)
            {
                if(ownerEmail == new UserEmail(userEmail))
                {
                    return Errors.Notes.NoteSharedToOwner;
                }

                var dbUser = await _userRepository.GetByEmailAsync(new UserEmail(userEmail), cancellationToken);
                if(dbUser is null)
                {
                    return Errors.Notes.NoteSharedToInvalidUser(userEmail.ToString());
                }

                if(SharedWith.Create(new UserEmail(userEmail), DateTime.UtcNow) is not SharedWith sharedWith)
                {
                    return Errors.Notes.SharedWithBadFormat;
                }

                if (sharedToUsers.Contains(sharedWith))
                {
                    return Errors.Notes.SharedWithDuplicatedUser;
                }

                sharedToUsers.Add(sharedWith);
            }

            var note = Note.Create(
                new NoteId(Guid.NewGuid()),
                command.Title.Trim(),
                command.Description.Trim(),
                ownerEmail,
                sharedToUsers,
                DateTime.UtcNow,
                DateTime.UtcNow
            );

            await _notesRepository.AddAsync(note.Value, cancellationToken);
            await _notesRepository.SaveChangesAsync(cancellationToken);

            return note.Value.Id.Value;
        }
    }
}
