﻿using ErrorOr;
using MediatR;

namespace Application.Notes.Create
{
    public record CreateNoteCommand(
        string Title, 
        string Description,
        List<string> SharedWith
    ) : IRequest<ErrorOr<Guid>>;
}
