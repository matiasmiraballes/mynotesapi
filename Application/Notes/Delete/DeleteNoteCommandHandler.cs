﻿using Application.Common.UserContext;
using Domain.DomainErrors;
using Domain.Notes;
using ErrorOr;
using MediatR;

namespace Application.Notes.Delete
{
    internal sealed class DeleteNoteCommandHandler : IRequestHandler<DeleteNoteCommand, ErrorOr<Unit>>
    {
        private readonly INotesRepository _notesRepository;
        private readonly IUserContext _userContext;
        public DeleteNoteCommandHandler(INotesRepository notesRepository, IUserContext userContext)
        {
            _notesRepository = notesRepository ?? throw new ArgumentNullException(nameof(notesRepository));
            _userContext = userContext ?? throw new ArgumentNullException(nameof(userContext));
        }

        public async Task<ErrorOr<Unit>> Handle(DeleteNoteCommand command, CancellationToken cancellationToken)
        {
            var dbNoteToDelete = await _notesRepository.GetUserNoteByIdAsync(new NoteId(command.noteId), cancellationToken);
            if(dbNoteToDelete is null)
            {
                return Errors.Notes.NotFound(command.noteId.ToString());
            }

            if(dbNoteToDelete.OwnerEmail != _userContext.UserEmail)
            {
                return Errors.Notes.Unauthorized;
            }

            await _notesRepository.DeleteUserNoteAsync(dbNoteToDelete, cancellationToken);
            await _notesRepository.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
