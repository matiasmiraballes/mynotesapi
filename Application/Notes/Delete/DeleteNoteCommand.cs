﻿using ErrorOr;
using MediatR;

namespace Application.Notes.Delete;

public record DeleteNoteCommand(Guid noteId) : IRequest<ErrorOr<Unit>>;
