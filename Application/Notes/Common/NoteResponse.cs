﻿namespace Application.Notes.Common;

public record NoteResponse(
    Guid Id,
    string Title,
    string Description,
    string OwnerEmail,
    List<string> SharedWith
);
