﻿using ErrorOr;
using MediatR;

namespace Application.Notes.Count;

public record GetNotesCountQuery() : IRequest<ErrorOr<int>>;