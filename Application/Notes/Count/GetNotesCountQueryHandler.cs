﻿using Domain.Notes;
using ErrorOr;
using MediatR;

namespace Application.Notes.Count
{
    internal sealed class GetNotesCountQueryHandler : IRequestHandler<GetNotesCountQuery, ErrorOr<int>>
    {
        private readonly INotesRepository _notesRepository;

        public GetNotesCountQueryHandler(INotesRepository notesRepository)
        {
            _notesRepository = notesRepository;
        }

        public async Task<ErrorOr<int>> Handle(GetNotesCountQuery request, CancellationToken cancellationToken)
        {
            return await _notesRepository.CountUserNotes(cancellationToken);
        }
    }
}
