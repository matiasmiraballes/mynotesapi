﻿using Application.Notes.Common;
using Domain.DomainErrors;
using Domain.Notes;
using ErrorOr;
using MediatR;

namespace Application.Notes.GetById
{
    internal sealed class GetNoteByIdQueryHandler : IRequestHandler<GetNoteByIdQuery, ErrorOr<NoteResponse>>
    {
        private readonly INotesRepository _notesRepository;

        public GetNoteByIdQueryHandler(INotesRepository notesRepository)
        {
            _notesRepository = notesRepository ?? throw new ArgumentNullException(nameof(notesRepository));
        }

        public async Task<ErrorOr<NoteResponse>> Handle(GetNoteByIdQuery request, CancellationToken cancellationToken)
        {
            NoteId noteId = new NoteId(request.id);
            Note? note = await _notesRepository.GetUserNoteByIdAsync(noteId, cancellationToken);

            if (note is null)
            {
                return Errors.Notes.NotFound(noteId.Value.ToString());
            }

            NoteResponse response = new(
                note.Id.Value,
                note.Title,
                note.Description,
                note.OwnerEmail.Value,
                note.SharedWith.Select(nw => nw.UserEmail.Value).ToList()
            );

            return response;
        }
    }
}
