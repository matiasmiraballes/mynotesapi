﻿using Application.Notes.Common;
using ErrorOr;
using MediatR;

namespace Application.Notes.GetById;

public record GetNoteByIdQuery(Guid id) : IRequest<ErrorOr<NoteResponse>>;
